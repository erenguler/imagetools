﻿using System;
using System.IO;
using ImageMagick;
using ImageMagick.ImageOptimizers;

namespace ImageTools.Optimizer
{
    class Program
    {
        public static int PRODX = 624, PRODY = 794; // original
        public static int PROD_X = 228, PROD_Y = 290;  // inlist
        public static int PROD__X = 118, PROD__Y = 150;  // thumnail

        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            int i = 1;

            // ana klasör --> adı images olmak zorunda
            string mainFolder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\images\";
            string outputFolder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\output" + i.ToString() + @"\";

            if (!System.IO.Directory.Exists(mainFolder))
            {
                Console.WriteLine("Lütfen masaüstüne 'images' adında ana klasörü oluşturun ve içerisine dönüştürülecek resimleri koyun...");
                return;
            }
            else
            {
                // ana klasördeki tüm dosyaları al
                string[] fileEntries = Directory.GetFiles(mainFolder);
                if (fileEntries.Length == 0)
                {
                    Console.WriteLine("'images' klaösöründe hiç resim bulunamadı...");
                    return;
                }

                // çıktı klasörü oluşturma
                while (System.IO.Directory.Exists(outputFolder))
                {
                    i++;
                    outputFolder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\output" + i.ToString() + @"\";
                }
                System.IO.Directory.CreateDirectory(outputFolder);

                // tüm resimlere işlemleri uygula
                foreach (string fullpath in fileEntries)
                {
                    try
                    {
                        string fileName = fullpath.Substring(fullpath.LastIndexOf(@"\"), (fullpath.Length - fullpath.LastIndexOf(@"\"))).Remove(0, 1);

                        using (MagickImage image = new MagickImage(fullpath))
                        {
                            image.Resize(PROD__X, PROD__Y);
                            image.Write(outputFolder + fileName);
                        }
                    }
                    catch { continue; }
                }

                Console.WriteLine(outputFolder + " klasörüne resimler başarıyla oluşturulmuştur...");
            }

        }
    }
}


//// JPEG2000
//using (MagickImage image = new MagickImage(fullpath))
//{
//    image.Resize(228, 290);
//    image.Write(outputFolder + fileName);
//    ImageOptimizer optimizer = new ImageOptimizer();
//    optimizer.LosslessCompress(outputFolder + fileName);
//    fileName = fileName.Replace(".jpg", ".jp2");
//    image.Format = MagickFormat.J2k;
//    image.Quality = 90;
//    image.Strip();
//    image.Write(outputFolder + fileName);
//    fileName = fileName.Replace(".jp2", ".jpg");
//    System.IO.File.Delete(outputFolder + fileName);
//}

//// SADECE BOYUT
//using (MagickImage image = new MagickImage(fullpath))
//{
//    image.Resize(228, 290);
//    image.Write(outputFolder + fileName);
//}

//// KALİTE AYARI
//using (MagickImage image = new MagickImage(fullpath))
//{
//    image.Resize(228, 290);
//    image.Quality = 30;
//    image.Write(outputFolder + fileName);
//}

//// OPTIMIZE
//using (MagickImage image = new MagickImage(fullpath))
//{
//  image.Resize(228, 290);
//  image.Quality = 35;
//  image.Write(outputFolder + fileName);
//  var imgInfo = new FileInfo(outputFolder + fileName);
//  var optimizer = new ImageOptimizer();
//  optimizer.OptimalCompression = true;
//  optimizer.LosslessCompress(imgInfo);
//  imgInfo.Refresh();
//}